package org.apdplat.realtimelog.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.apdplat.realtimelog.common.Constant;


public class TokenUtils {
	
    public static String generateToken(){
        return RandomStringUtils.generateRandomString(16);
    }
    
    public static String getLoginToken(HttpServletRequest request){
        return getCookieValue(Constant.LOGIN_TOKEN_KEY,request);
    }
    
    private static String getCookieValue(String name, HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        if(null == cookies){
            return "";
        }
        for(Cookie cookie : cookies){
            if(name.equals(cookie.getName())){
                return cookie.getValue();
            }
        }
        return "";
    }
}
