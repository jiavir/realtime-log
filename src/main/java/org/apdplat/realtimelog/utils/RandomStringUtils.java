package org.apdplat.realtimelog.utils;

import java.util.Random;

public class RandomStringUtils {
    
    private static final String BASE = "AaBbCcDdEeFfGgHhIiGgKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
    
    /**
     * 生成随机字符串
     * @param length
     * @return
     */
    public static String generateRandomString(int length) {
        StringBuffer sb = new StringBuffer("");
        int baseLength = BASE.length();
        // 控制字数
        for (int i = 0; i < length; i++) {
            String ch = BASE.charAt(new Random().nextInt(baseLength)) + "";  // 截取汉字
            sb.append(ch);
        }
        return sb.toString(); 
    }
    
    /**
     * 生成随机字符串
     * @param length
     * @return
     */
    public static String generateRandomString() {
        StringBuffer sb = new StringBuffer("");
        int baseLength = BASE.length();
        // 控制字数
        for (int i = 0; i < 3; i++) {
            String ch = BASE.charAt(new Random().nextInt(baseLength)) + "";  // 截取汉字
            sb.append(ch);
        }
        return sb.toString(); 
    }

}
