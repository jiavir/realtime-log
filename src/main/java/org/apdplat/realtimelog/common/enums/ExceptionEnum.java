package org.apdplat.realtimelog.common.enums;

public enum ExceptionEnum {
	SUCCESS("0", "成功"), 
	FAIL("1000", "操作失败，请联系客服")
	;
	
	private String code;
	private String msgZH;
	private String msgEN;
	private String msgJA;

	private ExceptionEnum(String msgZH) {
		this.msgZH = msgZH;
	}

	private ExceptionEnum(String code, String msgZH) {
		this.code = code;
		this.msgZH = msgZH;
	}

	private ExceptionEnum(String code, String msgZH, String msgEN, String msgJA) {
		this.code = code;
		this.msgZH = msgZH;
		this.msgEN = msgEN;
		this.msgJA = msgJA;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msgZH;
	}

	public String getMsg(int language) {
		switch (language) {
		case 0:
			return msgZH;
		case 1:
			return msgEN;
		case 2:
			return msgJA;
		default:
			return msgZH;
		}
	}
}
