package org.apdplat.realtimelog.server;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.apache.commons.lang.StringUtils;
import org.apdplat.realtimelog.server.ssh.SshClientTools;
import org.apdplat.realtimelog.utils.TimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@ServerEndpoint(value = "/websocket/{projectName}/{serviceName}/{level}")
@Component
public class LogWebSocket {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LogWebSocketHandler.class);
	
    static class ProcessInfo {
        private Process process;
        private InputStream inputStream;
        private String projectName;
        private String serviceName;
        private long start;
    }

    private final Map<String, ProcessInfo> processInfosBySession = new HashMap<>();
	
    //静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
    private static int onlineCount = 0;

    //concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
    private static CopyOnWriteArraySet<LogWebSocket> webSocketSet = new CopyOnWriteArraySet<LogWebSocket>();

    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;

    /**
     * 连接建立成功调用的方法*/
    @OnOpen
    public void onOpen(@PathParam(value="projectName") String projectName,
            @PathParam(value="serviceName") String serviceName,
            @PathParam(value="level") String level,EndpointConfig config,Session session) {
        this.session = session;
        webSocketSet.add(this);     //加入set中
        addOnlineCount();           //在线数加1
        System.out.println("有新连接加入！当前在线人数为" + getOnlineCount());
        try {
            sendMessage("CURRENT_WANGING_NUMBER"+projectName);
        } catch (IOException e) {
            System.out.println("IO异常");
        }
        //tail
        try {
            HttpSession httpSession= (HttpSession) config.getUserProperties().get(HttpSession.class.getName());
            String wsSessionId = session.getId();
            ProcessInfo processInfo = new ProcessInfo();
            processInfosBySession.put(wsSessionId, processInfo);
            /*
            //验证用户是否登录
            if(StringUtils.isBlank(getSessionProperty(httpSession, "userAccount"))){
                return;
            }
            */
            processInfo.projectName = projectName;
            processInfo.serviceName = serviceName;
            processInfo.start = System.currentTimeMillis();

            LOGGER.info("开始获取实时日志, projectName: {}, serviceName: {}, level: {}, wsSessionId: {}", projectName, serviceName, level, wsSessionId);

            if(StringUtils.isBlank(serviceName)){
                session.getBasicRemote().sendText("没有指定要查看日志的服务名" + "<br>");
                return;
            }
            if(StringUtils.isBlank(projectName)){
                session.getBasicRemote().sendText("没有指定要查看日志的服务所属的项目名称" + "<br>");
                return;
            }

            String commandPrefix = "";
            try {
                /*
                //验证项目是否存在
                ProjectRepository projectRepository = SpringContextUtils.getBean("projectRepository");
                Project project = projectRepository.findByName(projectName);
                if(project == null){
                    String info = "名称为"+projectName+"的项目不存在";
                    LOGGER.error(info);
                    session.getBasicRemote().sendText(info + "<br>");
                    return;
                }
                //验证服务是否存在
                //如果服务存在, 则能获取到该服务部署在那一台服务器上
                //DEV环境, 服务只会部署一份
                String deployServerIp = getDeployServerIp(projectName, serviceName);
                if(StringUtils.isBlank(deployServerIp)){
                    String info = "名称为"+serviceName+"的服务不存在";
                    LOGGER.error(info);
                    session.getBasicRemote().sendText(info + "<br>");
                    return;
                }
                //获取部署服务使用的用户名
                String deployServerUser = project.getDeployServerUser();
                //使用SSH远程获取服务的日志
                commandPrefix = "ssh " + deployServerUser + "@" + deployServerIp + " ";
                */
            }catch (Exception e){
                String info = "参数验证失败";
                LOGGER.error(info, e);
                session.getBasicRemote().sendText(info + "<br>");
                return;
            }
            //所有部署服务的服务器会使用规范的日志路径，格式为：/home/ubuntu/paas/logs/项目名称/服务名称.log
            //String command = commandPrefix+" tail -f /home/ubuntu/paas/logs/"+projectName+"/"+serviceName+".log";
            String command = commandPrefix+" tail -f "+projectName+"/"+serviceName+".log";

//            processInfo.process = Runtime.getRuntime().exec(command);

//            processInfo.inputStream = processInfo.process.getInputStream();

//            TailLogThread thread = new TailLogThread(processInfo.inputStream, session, level);
//            thread.start();
            
            try {
    			SshClientTools ssh = new SshClientTools("192.168.1.225", 22, "root", "root");
    			ssh.executeCommand("tail -20f /opt/www/server/timer-service-tomcat/logs/catalina.out", session, level);
    		} catch (Exception e) {
    			e.printStackTrace();
    			 LOGGER.error("获取实时日志，ssh异常", e);
    		}
        } catch (Exception e) {
            LOGGER.error("获取实时日志异常", e);
        }
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        webSocketSet.remove(this);  //从set中删除
        subOnlineCount();           //在线数减1
        System.out.println("有一连接关闭！当前在线人数为" + getOnlineCount());
        
        String wsSessionId = session.getId();
        if(!processInfosBySession.containsKey(wsSessionId)) {
            LOGGER.error("can not close invalid session id");
            return;
        }

        ProcessInfo processInfo = processInfosBySession.remove(wsSessionId);
        try {
            LOGGER.info("停止获取实时日志, 耗时: {}, projectName: {}, serviceName: {}, wsSessionId: {}",
                    TimeUtils.getTimeDes(System.currentTimeMillis()-processInfo.start), processInfo.projectName, processInfo.serviceName, wsSessionId);
            if(processInfo.inputStream != null) {
                processInfo.inputStream.close();
            }
            if(processInfo.process != null) {
                processInfo.process.destroy();
            }
        } catch (Exception e) {
            LOGGER.error("关闭实时日志流异常", e);
        }
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息*/
    @OnMessage
    public void onMessage(String message, Session session) {
        System.out.println("来自客户端的消息:" + message);

        //群发消息
        for (LogWebSocket item : webSocketSet) {
            try {
                item.sendMessage(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 发生错误时调用
     */
    @OnError
    public void onError(Session session, Throwable error) {
        System.out.println("发生错误");
        error.printStackTrace();
        LOGGER.error("获取实时日志异常", error);
    }


    public void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
        //this.session.getAsyncRemote().sendText(message);
    }

    private static String getSessionProperty(HttpSession session, String propertyName){
        try {
            return session.getAttribute(propertyName) == null ? null : session.getAttribute(propertyName).toString();
        }catch (Exception e){
            LOGGER.error("获取会话属性异常", e);
        }
        return null;
    }

    /**
     * 群发自定义消息
     * */
    public static void sendInfo(String message) throws IOException {
        for (LogWebSocket item : webSocketSet) {
            try {
                item.sendMessage(message);
            } catch (IOException e) {
                continue;
            }
        }
    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        LogWebSocket.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        LogWebSocket.onlineCount--;
    }
}
