package org.apdplat.realtimelog.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

	@RequestMapping("/{page}")
	public String page(@PathVariable String page, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return page;
	}
	
	@RequestMapping("/realtime-log")
	public String index(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
	    String serviceName = request.getParameter("serviceName");
	    if(StringUtils.isBlank(serviceName)){
	        response.getWriter().println("以参数serviceName指定要查看日志的服务名");
	        return "realtime-log";
	    }
	    String projectName = request.getParameter("projectName");
	    if(StringUtils.isBlank(projectName)){
	        response.getWriter().println("以参数projectName指定要查看日志的服务所属的项目名称");
	        return "realtime-log";
	    }
	    String level = request.getParameter("level") == null ? "debug" : request.getParameter("level").toLowerCase();
	    model.addAttribute("serviceName", serviceName);
	    model.addAttribute("projectName", projectName);
	    model.addAttribute("level", level);
		return "realtime-log";
	}
	
	@RequestMapping("/realtime-log/socket")
	public String socket(Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		String serviceName = request.getParameter("serviceName");
		if(StringUtils.isBlank(serviceName)){
			response.getWriter().println("以参数serviceName指定要查看日志的服务名");
			return "socket";
		}
		String projectName = request.getParameter("projectName");
		if(StringUtils.isBlank(projectName)){
			response.getWriter().println("以参数projectName指定要查看日志的服务所属的项目名称");
			return "socket";
		}
		String level = request.getParameter("level") == null ? "debug" : request.getParameter("level").toLowerCase();
		model.addAttribute("serviceName", serviceName);
		model.addAttribute("projectName", projectName);
		model.addAttribute("level", level);
		return "socket";
	}

}
