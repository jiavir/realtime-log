package org.apdplat.realtimelog.web.vo;

import java.io.Serializable;
import java.time.LocalDateTime;

public class LoginUser implements Serializable {

	private static final long serialVersionUID = 3890294230373456579L;

	private String userName;

	private String password;

	private Boolean master;

	private LocalDateTime loginTime;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getMaster() {
		return master;
	}

	public void setMaster(Boolean master) {
		this.master = master;
	}

	public LocalDateTime getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(LocalDateTime loginTime) {
		this.loginTime = loginTime;
	}
}
