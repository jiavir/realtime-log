### 微服务日志之实时日志

其他参考日志管理开源项目：
https://gitee.com/davegrohl/LogView
https://gitee.com/kailing/boot-websocket-log


非开源日志管理项目：
http://www.finderweb.net/finder

菜单权限管理系统：
https://gitee.com/sunxyz/sanji-boot?_from=gitee_search




   在微服务架构中，一般会有几百甚至几千个服务，这些服务可能会被自动部署到集群中的任何一台机器上，因此，开发人员在开发的时候，要想实时查看日志输出就很不方便了，首先需要查询出服务被部署到哪一台机器上了，其次要向管理员申请目标机器的访问权限，接着要用SSH登录到目标服务器上，使用tail -f来查看实时日志，而tail -f的功能很有限，使用起来也很不方便。这个开源项目就是为了解决微服务架构下日志的实时查看问题，使开发人员无需服务器权限就能获得强大灵活方便的查看实时日志的能力。

一、编译程序:

    mvn package


二、部署到Tomcat:

    cp target/realtime-log-0.0.1-SNAPSHOT.war ~/Downloads/apache-tomcat-8.5.32/webapps
     
三、启动Tomcat:

    cd ~/Downloads/apache-tomcat-8.5.32
    bin/catalina.sh start
    
四、调用测试接口生成日志：

    http://localhost:8080/realtime-log-0.0.1-SNAPSHOT/test/hello

五、查看实时日志：

    http://localhost:8080/realtime-log-0.0.1-SNAPSHOT/realtime-log.jsp?projectName=logs&serviceName=logback&level=debug
    
实际运行效果如下：

![src/main/resources/static/resource/DEBUG.png](src/main/resources/static/resource/DEBUG.png)
![src/main/resources/static/resource/INFO.png](src/main/resources/static/resource/INFO.png)
![src/main/resources/static/resource/WARN.png](src/main/resources/static/resource/WARN.png)
![src/main/resources/static/resource/ERROR.png](src/main/resources/static/resource/ERROR.png)
# realtime-log

#### 项目介绍
realtime-log 日志web页面

#### 软件架构
软件架构说明

主要技术
1.使用WebSocket建立长连接+ Runtime.exec()执行linux tail命令。

2.linux机器间建立信任。将需要查看日志的linux机器与部署日志监控服务的机器建立信任关系，以便使用linux 下的tail命令实时获取日志。

3. java使用ssh远程登录linux机器。如果查看日志的linux机器不能或者不想与部署日志监控服务的机器建立信任关系，就需要在代码中使用sshtools之类的工具，通过用户名、密码远程ssh登陆要查看日志的linux系统。 

应用方向
1.为部分不会使用linux系统查看业务日志的测试新秀提供方便。
2.稍加改造后，可以对线上系统进行异常日志监控报警。
3.在自动化测试中增加异常日志捕获，方便定位问题。
4.还没想出来，欢迎补充。


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)